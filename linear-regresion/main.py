import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets


# plt.interactive(False)


def signoid(z):
    return 1 / (1 + np.exp(-z))


def koszt(pred, y):
    return (-y * np.log(pred) - (1 - y) * np.log(1 - pred)).mean()


def spadek_gradientu(x, y, lr, iteracje):
    liczba_cech = x.shape[1]
    bias = 0
    wagi = np.zeros(liczba_cech)
    for n in range(iteracje):
        Z = np.dot(wagi, np.transpose(x) ) + bias
        prawdopodobienswto_klas = signoid(Z)
        dZ = prawdopodobienswto_klas - y
        dW = 1 / len(y) * np.dot(np.transpose(x), dZ)
        dB = 1 / len(y) * np.sum(dZ)

        wagi = wagi - lr * dW
        bias = bias - lr * dB
        if n % 10 == 0:
            print("Koszt: ", koszt(prawdopodobienswto_klas, y))

    return wagi, bias


iris = datasets.load_iris()
x = iris.data[:, :2]
y = (iris.target != 0) * 1
plt.scatter(x[:, 0], x[:, 1], c=y)
plt.show()
wagi, bias = spadek_gradientu(x, y, 0.2, 200)

Z = np.dot(wagi, np.transpose(x))+bias
prawdopodobienstwo_klas  = signoid(Z)

plt.scatter(x[:, 0], x[:, 1], c=prawdopodobienstwo_klas)
plt.show()

predykcja = []
for i in range(0,len(prawdopodobienstwo_klas)):
    if prawdopodobienstwo_klas[i] > 0.5:
        predykcja.append(1)
    else:
        predykcja.append(0)
plt.scatter(x[:, 0], x[:, 1], c=predykcja)
plt.show()